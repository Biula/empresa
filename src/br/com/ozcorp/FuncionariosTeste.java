package br.com.ozcorp;

/**
 * 
 * @author Fabiola Cruz
 *
 */

public class FuncionariosTeste {

	public static void main(String[] args) {
 		
		
		Funcionarios bia = new Funcionarios("Beatriz Souza", 
				"45638471863", 
				"9 553 558 6", 
				"091", 
				"beatriz@gmail.com", 
				new Cargo(new Departamento("D.F", "Financeiro"), "Analista", 5_000_00, 5),
				"951753",
				Sangue.O0,
				Sexo.FEMININO);
				
				
		System.out.println("DADOS DO FUNCIONARIO : ");
		System.out.println("NOME:          "+bia.getNome());
		System.out.println("SEXO:          "+bia.getSexo().s);
		System.out.println("TIPO SANGUINEO:"+bia.getSangue().sa);
		System.out.println("CARGO:         "+bia.getCargo().getTitulo());
		System.out.println("SALARIO:       "+bia.getCargo().getSalario());
		System.out.println("N�VEL DE ACESSO:"+bia.getCargo().getAcesso());
		System.out.println("DEPARTAMENTO:  "+bia.getCargo().getDepartamento().getDepartamento());
		System.out.println("CPF:           "+bia.getCpf());
		System.out.println("RG:            "+bia.getRg());
		System.out.println("N�MATRICULA:   "+bia.getMatricula());
		System.out.println("SENHA:         "+bia.getSenha());
		System.out.println("EMAIL:         "+bia.getEmail());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
			Funcionarios biula = new Funcionarios(   "Fab�ola Cruz Souza", 
										"061 278 618 88",
										"9 418 448 5",
										"096",
										"biula@gmail.com", 
										new Cargo(new Departamento("D.J", "Juridico"), "Engenheira", 3_000_00, 3),
										"258852",
										Sangue.O0, 
										Sexo.FEMININO );

		System.out.println("DADOS DO FUNCIONARIO : ");
		System.out.println("NOME:          "+biula.getNome());
		System.out.println("SEXO:          "+biula.getSexo().s);
		System.out.println("TIPO SANGUINEO:"+biula.getSangue().sa);
		System.out.println("CARGO:         "+biula.getCargo().getTitulo());
		System.out.println("SALARIO:       "+biula.getCargo().getSalario());
		System.out.println("DEPARTAMENTO:  "+biula.getCargo().getDepartamento().getDepartamento());
		System.out.println("CPF:           "+biula.getCpf());
		System.out.println("RG:            "+biula.getRg());
		System.out.println("N�MATRICULA:   "+biula.getMatricula());
		System.out.println("SENHA:         "+biula.getSenha());
		System.out.println("EMAIL:         "+biula.getEmail());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
				Funcionarios lu = new Funcionarios(   "Lucas Vieira Lopes Barbosa", 
										"381 456 618 38",
										"6 478 438 1",
										"100",
										"lucas@gmail.com",
										new Cargo(new Departamento("D.A", "Administrativo"),"Gerente", 10_000_00, 1),
										"259852",
										Sangue.O1, 
										Sexo.MASCULINO );

		System.out.println("DADOS DO FUNCIONARIO: ");
		System.out.println("NOME:          "+lu.getNome());
		System.out.println("SEXO:          "+lu.getSexo().s);
		System.out.println("TIPO SANGUINEO:"+lu.getSangue().sa);
		System.out.println("CARGO:         "+lu.getCargo().getTitulo());
		System.out.println("SALARIO:       "+lu.getCargo().getSalario());
		System.out.println("DEPARTAMENTO:  "+lu.getCargo().getDepartamento().getDepartamento());
		System.out.println("CPF:           "+lu.getCpf());
		System.out.println("RG:            "+lu.getRg());
		System.out.println("N�MATRICULA:   "+lu.getMatricula());
		System.out.println("SENHA:         "+lu.getSenha());
		System.out.println("EMAIL:         "+lu.getEmail());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
				Funcionarios di = new Funcionarios(   "Diogo Lopes", 
						"381 456 618 38",
						"6 478 438 1",
						"170",
						"lucas@gmail.com",
						new Cargo(new Departamento("D.O", "Operacional"),"Diretor", 10_000_00, 1),
						"259852",
						Sangue.AB1, 
						Sexo.FEMININO );
		
		System.out.println("DADOS DO FUNCIONARIO: ");
		System.out.println("NOME:          "+di.getNome());
		System.out.println("SEXO:          "+di.getSexo().s);
		System.out.println("TIPO SANGUINEO:"+di.getSangue().sa);
		System.out.println("CARGO:         "+di.getCargo().getTitulo());
		System.out.println("SALARIO:       "+di.getCargo().getSalario());
		System.out.println("DEPARTAMENTO:  "+di.getCargo().getDepartamento().getDepartamento());
		System.out.println("CPF:           "+di.getCpf());
		System.out.println("RG:            "+di.getRg());
		System.out.println("N�MATRICULA:   "+di.getMatricula());
		System.out.println("SENHA:         "+di.getSenha());
		System.out.println("EMAIL:         "+di.getEmail());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
				Funcionarios gi = new Funcionarios(   "Giulia Lopes", 
						"381 456 618 38",
						"6 478 438 1",
						"170",
						"lucas@gmail.com",
						new Cargo(new Departamento("D.A", "Administrativo"),"Secretario", 2_000_00, 1),
						"259152",
						Sangue.B0, 
						Sexo.OUTRO );
		
		System.out.println("DADOS DO FUNCIONARIO: ");
		System.out.println("NOME:          "+gi.getNome());
		System.out.println("SEXO:          "+gi.getSexo().s);
		System.out.println("TIPO SANGUINEO:"+gi.getSangue().sa);
		System.out.println("CARGO:         "+gi.getCargo().getTitulo());
		System.out.println("SALARIO:       "+gi.getCargo().getSalario());
		System.out.println("DEPARTAMENTO:  "+gi.getCargo().getDepartamento().getDepartamento());
		System.out.println("CPF:           "+gi.getCpf());
		System.out.println("RG:            "+gi.getRg());
		System.out.println("N�MATRICULA:   "+gi.getMatricula());
		System.out.println("SENHA:         "+gi.getSenha());
		System.out.println("EMAIL:         "+gi.getEmail());


		
	}
}
