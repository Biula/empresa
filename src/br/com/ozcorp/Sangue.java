package br.com.ozcorp;
/**
 * 
 * @author Fabiola Cruz
 *
 */

public enum Sangue {
	
	/*
	 * 0 - NEGATIVO
	 * 1 - POSITIVO
	 */
	 
	A1("A+"), A0("A-"), B1("B+"), B0("B-"), AB1("AB+"), AB0("AB-"), O1("O+"), O0("O-");

	public String sa;

	Sangue(String sa) {
		this.sa = sa;
	}
}
