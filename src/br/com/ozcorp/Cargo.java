package br.com.ozcorp;
/**
 * 
 * @author Fabiola Cruz
 *
 */

public class Cargo {
	private String titulo;
	private double salario;
	private int acesso;
	private Departamento departamento;

	// CONSTRUTOR
	public Cargo(Departamento departamento, String titulo, double salario, int acesso) {
		this.departamento = departamento;
		this.titulo = titulo;
		this.salario = salario;
		this.acesso = acesso;
	}

	//GETTERS E SETTERS
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public int getAcesso() {
		return acesso;
	}

	public void setAcesso(int acesso) {
		this.acesso = acesso;
	}

}