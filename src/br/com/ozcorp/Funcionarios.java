package br.com.ozcorp;

/**
 * 
 * @author Fabiola Cruz
 *
 */
public class Funcionarios {

	// ATRIBUTOS
	private String nome;
	private String cpf;
	private String rg;
	private String matricula;
	private String email;
	private Cargo cargo;
	private String senha;
	private Sangue sangue;
	private Sexo sexo;

	// CONSTRUTOR

	public Funcionarios(String nome, String cpf, String rg, String matricula, String email, Cargo cargo, String senha,
			Sangue sangue, Sexo sexo) {

		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.matricula = matricula;
		this.email = email;
		this.cargo = cargo;
		this.senha = senha;
		this.sangue = sangue;
		this.sexo = sexo;

	}

	// getter e setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;

	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Sangue getSangue() {
		return sangue;
	}

	public void setSangue(Sangue sangue) {
		this.sangue = sangue;
	}

}


