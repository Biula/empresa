package br.com.ozcorp;
/**
 * 
 * @author Fabiola Cruz
 *
 */

public class Departamento {
	private String sigla;
	private String departamento;
	
	
	//CONSTRUTOR
	public Departamento(String sigla, String departamento) {
		super();
		this.sigla = sigla;
		this.departamento = departamento;
		
	}

	//GETTERS E SETTERS

	public String getSigla() {
		return sigla;
	}


	public void setSigla(String sigla) {
		this.sigla = sigla;
	}


	public String getDepartamento() {
		return departamento;
	}


	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}



	
	
	
	
	
	
	
	
	

}
